/* eslint-disable @typescript-eslint/no-var-requires */
// eslint-disable-next-line no-unused-vars
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const router = require('express').Router();
const {labels, todos,} = require('./mocks.js');

const ErrorHandler = (error, _req, res) => {
  const statusCode = res.statusCode || 500;
  res.status(statusCode);
  res.json({
    message: error.message,
    stack: process.env.NODE_ENV === 'production' ? '🥑' : error.stack,
  });
};

try {
  router.post('/signup', (_req, _res, next) => {
    setTimeout(next, 1000);
  }, (_req, res) => {
    res.status(200).send({accessToken: 'jhbdhbfsuhbvhsfbvuhsbfuvhsbfu',});
  });

  router.post('/login', (_req, _res, next) => {
    setTimeout(next, 1000);
  }, (req, res) => {
    if(req.body.password !== 'pass') res.status(401).send('Wrong password!');
    else res.status(200).send({accessToken: 'lfhjvbfealihvbsfjlh',});
  });

  router.get('/user', (_req, res) => {
    res.send({
      username: 'stalin',
      email: 'vozhd@kgb.ussr',
    });
  });

  router.put('/user/username', (req, res) => {
    if(req.body.username === 'error') {
      res.status(500).send();
    } else {
      res.status(201).send();
    }
  });

  router.put('/user/password', (_req, res) => {
    res.status(201).send();
  });

  router.get('/labels', (_req, res) => {
    res.send(labels);
  });

  router.patch('/label', (_req, res) => {
    res.status(201).send();
  });

  router.put('/labels', (_req, res) => {
    res.status(201).send();
  });
  
  router.get('/todos', (_req, res) => {
    res.send({todos,});
  });

  router.post('/todo',  (_req, _res, next) => {
    setTimeout(next, 1000);
  }, (req, res) => {
    if(req.body.title === 'error') {
      res.status(500).send();
    } else {
      res.status(204).send();
    }
  });

  router.put('/todo',  (_req, _res, next) => {
    setTimeout(next, 300);
  }, (req, res) => {
    if(req.body.todo.title === 'error' || req.body.todo.title === 'Неудаляемая') {
      res.status(500).send();
    } else {
      res.status(201).send();
    }
  });

  router.delete('/todo',  (_req, _res, next) => {
    setTimeout(next, 300);
  }, (req, res) => {
    if(req.body.id === 3) {
      res.status(500).send();
    } else {
      res.status(204).send();
    }
  });

  router.patch('/todo', (_req, res) => {
    res.status(204).send();
  });
  
  router.use(ErrorHandler);
} catch(e) {
  console.error(e);
}

module.exports = router;
 