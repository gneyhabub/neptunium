/// <reference types="cypress" />

context('Actions', () => {
	beforeEach(() => {
		cy.visit('/')
	})
	it('.type() - type into a DOM element', () => {
    cy.get('input[name="userName"]')
      .type('putin').should('have.value', 'putin')

    cy.get('input[data-cy="password"]')
      .type('putin', { force: true })
		.should('have.value', 'putin')
	cy.get('.button').click()

	})
	it( 'cypress studio', () => {
	})
})