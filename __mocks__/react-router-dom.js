module.exports = {
  useHistory: () => ({
    push: () => {},
  }),
  NavLink: () => ('Link'),
  Switch: () => ('Switch'),
  Route: () => ('Route'),
  BrowserRouter: () => ('BrowserRouter'),
  Redirect: () => ('Redirect'),
  useParams: () => ({id: '1',}),
};