/* eslint-disable @typescript-eslint/no-var-requires */
const pkg = require('./package');
const path = require('path');

module.exports = {
  'apiPath': 'stubs/api',
  'webpackConfig': {
    'output': {
      'publicPath': `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`,
    },
    'module': {
      'rules': [
        {
          'test': /\.s[ac]ss$/i,
          'use': [
            'style-loader',
            'css-loader',
            {
              'loader': 'postcss-loader',
              'options': {
                'postcssOptions': {
                  config: path.resolve(__dirname, 'postcss.config.js'),
                },
              },
            }
          ],
        }
      ],
    },
  },
  'config': {
    'neptunium.api': '/api',
  },
  'navigations': {
    'neptunium': '/neptunium',
    'link.neptunium.login': '/login',
    'link.neptunium.signup': '/signup',
    'link.neptunium.home': '/home',
    'link.neptunium.home.list': '/home/list',
    'link.neptunium.home.settings': '/home/settings',
    'link.neptunium.home.list.new': '/home/list/new',
    'link.neptunium.home.list.edit': '/home/list/edit',
  },
};
