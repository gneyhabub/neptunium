export interface Label {
  title: string;
  color: string;
}

export interface Todo {
  id: string;
  labels: Label[];
  completed: boolean;
  title: string;
  description: string;
  completeDate: Date;
}