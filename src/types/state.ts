import { Todo } from './todos';

export interface State {
  todos: Todo[]
  username: string;
  accessToken: string;
}