import React from 'react';
import HomePage from './components/HomePage/HomePage';
import { Switch, Route, BrowserRouter, Redirect } from 'react-router-dom';
import Register from './components/Authentication/Register/Register';
import NotFound from './components/NotFound/NotFound';
import Login from './components/Authentication/Login/Login';
import { baseUrl, urls } from './__data__/constants/urls';
import { useSelector } from 'react-redux';
import { loginSelectors } from './__data__/slices/login';


export default function BaseRouter() {
  let loggedIn = useSelector(loginSelectors.accessToken);
  if(!loggedIn) {
    loggedIn = localStorage.getItem('accessToken');
  }
  return (
    <div>
      <BrowserRouter basename={baseUrl}>
        <Switch>
          <Route
            exact
            path="/"
          > 
            {loggedIn ? (<Redirect to={urls['home.list']} />) : (<Redirect to={urls.login} />)}
          </Route>
          <Route
            exact
            path={urls.signup}
            component={Register}
          />
          <Route
            exact
            path={urls.login}
            component={Login}
          />
          {loggedIn && <Route
            path={urls.home}
            component={HomePage}
          />}
          <Route
            component={NotFound}
          />
        </Switch>
      </BrowserRouter>
    </div>
  );
}
