/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React from 'react';
import ReactDom from 'react-dom';
import * as Sentry from '@sentry/react'; 
import { Integrations } from '@sentry/tracing';

import App from './app';

export default () => <App/>;

Sentry.init({
  dsn: 'https://21131698a195410998e08f2b6c2112cf@o562487.ingest.sentry.io/5701078',
  integrations: [new Integrations.BrowserTracing()],
  tracesSampleRate: 1.0,
});

export const mount = (Сomponent) => {
  ReactDom.render(
    <Сomponent/>,
    document.getElementById('app')
  );

  if(module.hot) {
    module.hot.accept('./app', ()=> {
      ReactDom.render(
        <App/>,
        document.getElementById('app')
      );
    });
  }
};

export const unmount = () => {
  ReactDom.unmountComponentAtNode(document.getElementById('app'));
};

