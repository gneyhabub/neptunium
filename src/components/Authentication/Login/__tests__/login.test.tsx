import React from 'react';
import {describe, expect, beforeEach, it} from '@jest/globals';
import {mount} from 'enzyme';
import Login from '../Login';
import { Provider } from 'react-redux';
import store from '../../../../__data__/store';
import mockAdapter from 'axios-mock-adapter';
import { act } from 'react-dom/test-utils';
import axios from 'axios';

const multipleRequest = async (mock, responses) => {
  await act(async () => {
    await mock.onAny().reply((config) => {
      const [url, ...response] = responses.shift();

      if(config.url.includes(url)) {
        return response;
      }

    } );
  });
};

describe('Login', () => {
  const component = mount(<Provider store={store}><Login/></Provider>);
  describe('login to the system', () => {
    let mockAxios;
    beforeEach(() => {
      mockAxios = new mockAdapter(axios);
    });
    it('renders', async () => {
      expect(component).toMatchSnapshot();
      component.find('button').simulate('click');
      expect(component.find('button')).toMatchSnapshot();

      const response = [
        [
          'POST',
          '/login',
          {},
          200,
          {
            accessToken: 'token',
          }
        ]
      ];

      await multipleRequest(mockAxios,response);
      component.update();

      expect(component).toMatchSnapshot();
    });
  });
  describe('components', () => {
    const signup = '/signup';
    it('should have btn', () => {
      expect(component.find('button')).toHaveLength(1);
    });
    it('should have link to registration', function () {
      const navlink = component.find('NavLink');
      expect(navlink).toHaveLength(1);
      expect(navlink.get(0).props.to).toBe(signup);
      expect(navlink.get(0).props.children).toBe('First time? Sign up here!');
    });
    it('should have two inputs', function () {
      expect(component.find('input')).toHaveLength(2);
    });
  });
});
