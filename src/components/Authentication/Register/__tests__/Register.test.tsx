/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import {describe, it, jest, expect} from '@jest/globals';
import { mount } from 'enzyme';
import Register from '../Register';
import { useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';


const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));
jest.mock('react-hook-form', () => ({
  useForm: jest.fn(),
}));

describe('Register', () => {
  describe('when data in storage is provided', () => {
    it('matches the snapshot', () => {
      (useForm as any).mockImplementation(() => ({
        register: () => {},
        handleSubmit: (fn) => {
          fn({username: 'username', email: 'email', password: 'password' ,}); 
        },
        watch: () => {},
        errors: {
          passwordRepeat: {
            message: 'Error',
          },
          password: {
            message: 'Error',
          },
          email: {
            message: 'Error',
          },
          username: {
            message: 'Error',
          },
        },
      }));
      (useSelector as any).mockImplementation(() => {
        return 'test';
      });
      const component = mount(<Register/>);
      expect(component).toMatchSnapshot();
    });
  });
  
  describe('when data in storage is NOT provided', () => {
    it('matches the snapshot', () => {
      (useForm as any).mockImplementation(() => ({
        register: () => {},
        handleSubmit: (fn) => {
          fn({username: 'username', email: 'email', password: 'password' ,}); 
        },
        watch: () => {},
        errors: {
          passwordRepeat: {
            message: null,
          },
          password: {
            message: null,
          },
          email: {
            message: null,
          },
          username: {
            message: null,
          },
        },
      }));
      (useSelector as any).mockImplementation(() => {
        return null;
      });
      const component = mount(<Register/>);
      expect(component).toMatchSnapshot();
    });
  });
});