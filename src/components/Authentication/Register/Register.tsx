
import React from 'react';
import {NavLink, useHistory} from 'react-router-dom';
import '../Authentication.scss';
import { urls } from '../../../__data__/constants/urls';
import {useForm} from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import loginAPI from '../../../__data__/api/login';
import { loginSelectors } from '../../../__data__/slices/login';


export default function Register() {
  const { register, errors, handleSubmit, watch, } = useForm();
  const history = useHistory();
  const password = React.useRef({});
  const dispatch = useDispatch();
  password.current = watch('password', '');
  const error = useSelector(loginSelectors.error);
  const accessToken = useSelector(loginSelectors.accessToken);

  const onSubmit = ({username, email, password,}) => {
    dispatch(loginAPI.signup(username, password, email));
  };
  React.useEffect(() => {
    if(accessToken) {
      history.push(urls['home.list']);
    }
  }, [accessToken]);
  return (
    <div className="auth-wrapper">
      <div className="LoginBar center">
        <div className="company_name">
          <div className="logo">
            <span className="neptunium">Neptunium</span>
            <img className="planet-icon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAALxUlEQVR4nO2debBcRRXGv84G2UhCWEISICGswbBFLBEREEQQUQQKCMomSFEsilpCoWxFsQVkUSRsJRYqLhRuqBg0agowBYadEEggCRAQkmDykpeQ7b38/OPcqQx9l5m5fefNxLpf1VQyb+79zunbfbtPnz59WipRokSJEiVKlChRokSJEiVKlChRokSJEps+XKsVqAdAb0kTJH1c0q6SdpI0TtIQSYMkDZaEpJWSlktaJek1SXOjf2dKesk5tyFAh2HRfwdI2qzqp05JHc659Xm5q9G2FQKMlHSspC9K+oTswYdgqaQnJE2V9JBzboknb5CkvSTtI2kXSSMljZI0WtJ2kvrV4F8kab6kOZKekfSkpGcbbQRtVSFAP0knSDpX0oGSejVJVJekabKHNl5WCTs3Qd4SSY9Iul/SdOcctW5oiwoBBkv6pqTzJG3bYnWahbmSrpX0gHOuO+2illYIsJmk8yVdKmmrGpe/J+lxSS/LuoW5kpZJ6pCNHb0lDYs+Q2XdzH6SJkafWvy1sEo2Pi2XtEY2fklSf0kjVP+zfFHS2c65mYH6FAvgk8BssjEDuBAYX4C8XYHvAi/UkAnwd+As4NPAWKBvDe5+wE7AYcAVwDRgZQZ/F/Cd0DIVAmBz4A5gQ4qyy4DrgF2bqMN44EagM0WHbuBWYECAjL7A4cDPgXUpcu4CWtdLRa3o2RTlFgOXAlv0oD5bRZW/IkOn+4Djgf4BcsYAP02RcVORZWpEqcOw1p/UGqew0c5vhW7DgXtJf2sBFmFd3pDajKlyjgKWJnAfV2R56lHkJGBNgiKzgY/2qDIZAD4LvJlRKWBvTe4HCOwOvOdxLumxBgmcHb0FPu4HBvaIEg0A2ALr97OwAbgkQMZEYK3HeW2R5UgTfCJmUVSjCzi36cIDADjgSrK7MIAzAmRc4XEtwaYBzQFmYfitYA093V8GADg1oQzV6ARG5+QeCiz3+I4sugwVYWOA9z1hq4HDmiKwiQCOJt1sBbgtgPsuj+vGInWvCOlP3LTt2pTeDB/A6aR3XyvJORYCX/a4phatu7AJlY+2HjPqAXBxxltyaE7O8R7PM0UrfQBxi+r+QoW0EKRP7nK5QYAdPZ5ZRSrbl7hvajZtaNrmBWYSL0iokGty8o30eOYW6f8/R9IeVd83SDrNObeqQBkthXNuhaRTJfnu80znYwa2874vLaRCsPWMy70/3+2ce7oI/naCc+4JSb/2/vxuTrqdve/zinpDzteHF5aWSPpeQdztiFHe99dz8uxTEM9GRGPHQq8vvDSYuE2BTeiq5yXdwNY5ufy1maOLUPBkj3QZAR7Rdgdwvlfep3Ly7ODxrAYGFNFlne19v8s5t7wA3rYD0EvWPVfjwZx0vh9sunPug5xcBmAb4s7Dpq30tRrAVxJa9fAcPP2A/3hcXy1CwXM90hnBpG0KYADxOcg9Obl8l8lSApaLq4kf9ogvDCZtUwA3e2X9gBye3ujteM3jurkIBXsB//WI96h956YH4EDiXfMNObm+7vGsA3YqQskJHvG7tDKCokkAtgXe9sr6Wp4uBtgSW4iqxo+KULIf8IhH7M9eN3lE5ZzulbMbOCQnn++cXAFsE6pkH+C3xHFlEHGbAVvGfSChnFfn5Pt8AtdloUr2Bn6ZQAxwchB5mwG4JaGMU7G5SKNcw4l3ey9hweW5FewF/CSlMgD2y03eZgBuSijfbGDLHFy9gL94XF3A/qFK3pZRGQA7BgloA2DdVNKK5zt5ywdclcA3OVTRs2pUBuSYsbYTsAE8acxYAuyVk/NzxFdQ/01IuA+2LOtHHc5LUDx/f9hiAMOwiHcfiwMqYz/iMcNLgTEhio4i7nPpwBbn13t/b16QVxOBzaf8mTPYvGpCTs4xCc+tCzgqRNHNgKc80vXAEdHv/v6HMHu6BQBOSSgHwKvA2Jycw4FXEjgvClX2mgTSC6LfBhEPHvOXI9sWwGDSLcZ/kXM8jCrjuQTOKaEK70u8S3qo6vfjE4S2TRR7FrAxMamLAribnF1vRmX8jBxzl2rivsDzHulbVNngJMcnTcottAeAvdU/IDkSfzVwVgD3cJI3Iv0G6BOq+OUeaTdwsHdN0h6KXLFJPQHgC8AbCToDzCKnJRVxbw+8nMD7CKGWJ7bB0Tdxp3jXDCA5zvX3QcKbAMwa/GtKRWzA9jqGbFXbk3iAB8A/Q3irBfgbVRbj7ewhHm1XwdvBChSESMcpxMfBCl4nMBIf20WctD0NLAtFcCH2JN6/xtZ5gREpSkBOu70oAFsD38dW85KwHvNTBS2XAmeSvEUPYE5RhfmxR/wyCdYB5mJIa3kt2XuNTcR+CKzKaCzTgI8EyumDGQZZyLXO7gvaOqHGT8u43rfCKpgerExjeh+I+Z/SGgiYifulAmRtRbJ7xccJRRTMX+ddQEb2AuDqFGW6yTnDbUDX4cAFwIs1HsxbwDlZ5WhA5kERXy10UlD0yEyP+OIa14/GbPckFG7+AkOAScAfyd5eVqmIbwCbFyC3NzYN8AMcwPat+z3Fr4oo7Ag+bMZ2AX4wcdJ9/nylgncINPmiB7Ev8G2sm6hVCWBj3hkU5HXGHKv/SJE1A9iL+JgVvmmTeDReXcFu2ErY1BSFO7CUFCcBO5MRjYL5lSZE116HTag66qgAsLHjd8ARhLgn4jqdTrpJewdm2Pjd9uyscjYi3Lcarmrg3oHAn+p4cKuwCdQLwNPRv/MyCl0Lr2BvaK4tyRnlGZVRnmXAidF1I4gnrilmHyVxy+GYBu/vg22yT7P9i8JcbI4xsZCCf7gMDlsVTcrFAvAYsEPV9fd6v79JAWNWhdz3fI7LyTMW23edllmnUSzFQlW/RXNTNe0DPJGiwzrgMiwJZ+X6g4m7jk4vUiE/km5oIN8gLKXGFMwKyZqsgWVKmA/8DQu9ORMbMJuVb7Gi51DgdpItKLCudW/vnsHEG/BzhepKvB8fXBj5RhkjMNfMRGwyNxHLijOy2Q8+QZe+2PxkUUpFrAYuIcFtTjwIYj1Fd6HEXemFDpTtAswqPCWhhVdjGrBbyv0XJVx/XTMUneEJ+UzhQloIbMA+huxciwuJLKgUjuMoOpwnQ9idnqDm523qAWDW3ySSl1UrWA1cT0ZyAyy2yvfzLabK6ipa8UmesPmELju2EFjym/OicqRhXdQQMz0S2EqjXxlryZnbpN4CDCbulzqzaQKbBGAcMDlqvWnoxoIOapr2mMM1KflauDe3DuH3eIKX1aN0q4H5vI7FXDhJwQvVFfEgdayHYEvU96VwhG/MrAdYVhr/1XwBaMu038D+WFC0Hx3oY03U2HZpgDcpYGEtPb3dAsuZ62NBPa2qJwDsjblo5tSoBLA3fDLgJ3dJ4x6GrTgmTRKXA4c3u3xJSvUG/pzSym6gCRPGGvr0x9Lr3Unt1K0VzMR8UnUtEmHO0YtJd3I+R51vV1OAuT0eTVFuMRYgsHuTZA/BTMzrMd9SVvLJanRi8QB1b4DBMmxPzqiISohQMQ7DBhDz32NLnndLyrK0ZkmaLukxWbb/Bc65dfUIxOJmR8tSE02IPntLGqv6z+9YL+lRSb+Q9Id6UlJgW7aPkHSipAOUfprBq5LOcc49XqcuhSJRKWyh5TRJk1XfeR4bJC2U9L7s+Ii1suMdBsmSew2TNFDSjrLjHULwuqRbZMcYLZIdVdEpCUlbSNpc0g6StpclVNtX0scUT6nk431ZeW93zq0N1DE3Mle4sKw+V0j6muycp/9HLJZ0h6RbnXOdrVamriVH7HymSbLMP/vXe1+DWCnp+eizm6RDJTXLY4CkGZLulJ1H1bI3wkfDDxZL1vUpSYfIuoNxshNm6kWHpDclvSFpnqwCnpE0p/oooGisOVbSkZIOUvhRSJ2ySnhYNu68E8jXFBTS0jHn3FhZtzZQG8eOdbKxZJXsDVjonOvIKWM32du5u+wUtXHaeLzRIFlZVkpaIav0+ZIWyI5GelLSrKyzn0qUKFGiRIkSJUqUKFGiRIkSJUqUKFGiRIni8D8blHpb8QMr/QAAAABJRU5ErkJggg==" />
          </div>
          <span className="reg">Create an account</span>
        </div>
        <div className="login-form">
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="input_center">
              <span className="text">Username</span>
              <input 
                className="auth_input"
                ref={register({
                  required: 'Username is required!',
                })}
                name="username"
                style={{
                  border: errors.username && errors.username.message ? '1px red solid' : '',
                  outline: 'none',
                }}
              />
              
            </div>

            <div className="input_center">
              <span className="text">Email</span>
              <input 
                className="auth_input"
                ref={register({
                  required: 'Email is required!',
                })}
                name="email"
                style={{
                  border: errors.email && errors.email.message ? '1px red solid' : '',
                  outline: 'none',
                }}
              />
              
            </div>

            <div className="input_center">
              <span className="text">Password</span>
              <input 
                type='password' 
                className="auth_input"
                ref={register({
                  required: 'Password is required!',
                })}
                name="password"
                style={{
                  border: errors.password && errors.password.message ? '1px red solid' : '',
                  outline: 'none',
                }}
              />
              
            </div>

            <div className="pass_again input_center">
              <div className="input_center">
                <span className="text">Repeat password</span>
                <input 
                  type='password' 
                  className="auth_input"
                  ref={register({
                    required: 'Password is required!',
                    validate: value =>
                      value === password.current || 'The passwords do not match',
                  })}
                  name="passwordRepeat"
                  style={{
                    border: errors.passwordRepeat && errors.passwordRepeat.message ? '1px red solid' : '',
                    outline: 'none',
                  }}
                />
              </div>
              
            </div>

            {error && <p className="error">{error}</p>}

            <button type="submit" className="button">Sign Up</button>
          </form>
          <div className='already_registered'>
            <NavLink className='already_registered_text' to={urls['login']}>Already have an account? Sign In</NavLink>
          </div>
        </div>
      </div>
    </div>
  );
}
