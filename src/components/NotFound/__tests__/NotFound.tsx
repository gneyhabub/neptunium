import {mount} from 'enzyme';
import React from 'react';
import {describe, it, expect} from '@jest/globals';
import NotFound from '../NotFound';

describe('NotFound', () => {
  it('matches the snapshot', () => {
    const component = mount(<NotFound />);
    expect(component).toMatchSnapshot();
  });
});