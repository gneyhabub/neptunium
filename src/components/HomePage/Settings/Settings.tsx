import React from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { labelsSelectors } from '../../../__data__/slices/labels';
import { loginSelectors } from '../../../__data__/slices/login';
import labelsAPI from '../../../__data__/api/labels';
import loginAPI from '../../../__data__/api/login';
import './Settings.scss';

export default function Settings() {
  const {register, errors, handleSubmit, watch, formState: {isSubmitted, isDirty,},} = useForm();
  const password = React.useRef({});
  password.current = watch('password', '');
  const dispatch = useDispatch();
  const labels = useSelector(labelsSelectors.labels);
  const accessToken = useSelector(loginSelectors.accessToken);
  const userName = useSelector(loginSelectors.userName);
  React.useEffect(()=>{
    if(!labels.length) dispatch(labelsAPI.getLabels(accessToken));
  }, []);

  const onSubmit = (data) => {
    const requestBody = {
      username: null,
      password: null,
      labels: null,
    };
    if(data.username !== userName) {
      requestBody.username = data.username;
    }
    if(data.password) {
      requestBody.password = data.password;
    }

    let labelsDirty = false;
    labels.forEach(label => {
      if(!data[label.color]) data[label.color] = null;
      if(data[label.color] !== label.title) {
        labelsDirty = true;
      }
    });
    if(labelsDirty) {
      requestBody.labels = labels.map(label => ({color: label.color, title: data[label.color],}));
    }

    dispatch(loginAPI.updateUserData(accessToken, requestBody));
  };

  return (
    <div className="Settings">
      <h1 className="Settings_header">Settings</h1>

      <form className="Settings_form" onSubmit={handleSubmit(onSubmit)}>
        <div className="Settings_form-fields">
          <div className="Settings_first-column">
            <div className="Settings_text-input Settings_username">
              <label htmlFor="usrName" className="Settings_label">User Name</label>
              <input 
                id="usrName"
                type="text"
                className="Settings_box"
                name="username"
                defaultValue={userName}
                ref={register({
                  required: 'Username cannot be empty!',
                })}
                style={{
                  border: errors.username && errors.username.message ? '1px red solid' : '',
                  outline: 'none',
                }}
              />
              {errors.username && <p className="Settings_error">{errors.username.message}</p>}
            </div>
            <div className="Settings_text-input">
              <label htmlFor="usrName" className="Settings_label">New password</label>
              <input 
                id="password1"
                type="password"
                className="Settings_box"
                name="password"
                ref={register()}
              />
              
            </div>
            <div className="Settings_text-input">
              <label htmlFor="usrName" className="Settings_label">Repeat new password</label>
              <input 
                id="password2"
                type="password"
                className="Settings_box" 
                name="passwordRepeat"
                ref={register({
                  validate: value =>
                    value === password.current || 'The passwords do not match',
                })}
                style={{
                  border: errors.passwordRepeat && errors.passwordRepeat.message ? '1px red solid' : '',
                  outline: 'none',
                }}
              />
              {errors.passwordRepeat && <p className="Settings_error">{errors.passwordRepeat.message}</p>}
            </div>
          </div>
          <div className="Settings_second-column">
            <p className="Settings_tags-name">Tags</p>
            <div className="Settings_tags">
              {labels && labels.map((label) => (
                <input 
                  key={label.color}
                  className="Settings_tag"
                  type="text"
                  name={label.color}
                  ref={register()}
                  defaultValue={label.title}
                  style={{
                    backgroundColor: `#${label.color}`,
                  }}
                />
              ))}
            </div>
          </div>
        </div>
        <div className="Settings_submit">
          <button 
            type="submit"
            disabled={!isDirty}
            style={{
              backgroundColor: !isDirty ? 'grey' : '',
            }}
          >
            {isSubmitted ? 'Done!' : 'Save'}
          </button>
        </div>
      </form>
    </div>
  );
}
