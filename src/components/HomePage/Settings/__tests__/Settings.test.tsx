/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import {describe, it, jest, expect} from '@jest/globals';
import { mount } from 'enzyme';
import Settings from '../Settings';
import { useSelector } from 'react-redux';
import { labelsSelectors } from '../../../../__data__/slices/labels';
import { loginSelectors } from '../../../../__data__/slices/login';
import { useForm } from 'react-hook-form';

const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));

jest.mock('react-hook-form', () => ({
  useForm: jest.fn(),
}));

describe('Settings', () => {
  describe('when labels and username are provided', () => {
    describe('when they are the same as the form data', () => {
      it('matches the snapshot', () => {
        (useForm as any).mockImplementation(() => ({
          register: () => {},
          handleSubmit: (fn) => {
            fn({username: 'username', '0B8043': 'Дача', '0C9CE5': 'Aниме',}); 
          },
          watch: () => {},
          errors: {
            username: {
              message: 'Error',
            },
            passwordRepeat: {
              message: 'Error',
            },
          },
          formState: {
            isSubmitted: true,
            isDirty: true,
          },
        }));
        (useSelector as any).mockImplementation((selector) => {
          if(selector === labelsSelectors.labels) {
            return [{
              'color':'0B8043',
              'title':'Дача',
            },
            {
              'color':'0C9CE5',
              'title':'Онеяме',
            },
            {
              'color':'DICK',
              'title':'Аниме',
            }];
          } else if (selector === loginSelectors.userName) {
            return 'username';
          }
          else {
            return 'test';
          }
        });
        const component = mount(<Settings/>);
        expect(component).toMatchSnapshot();
      });
    });
    describe('when they are NOT the same as the form data', () => {
      it('matches the snapshot', () => {
        (useForm as any).mockImplementation(() => ({
          register: () => {},
          handleSubmit: (fn) => {
            fn({username: 'username', password: 'password', '0B8043': 'Дача', '0C9CE5': 'Aниме',}); 
          },
          watch: () => {},
          errors: {
            username: {
              message: null,
            },
            passwordRepeat: {
              message: null,
            },
          },
          formState: {
            isSubmitted: false,
            isDirty: false,
          },
        }));
        (useSelector as any).mockImplementation((selector) => {
          if(selector === labelsSelectors.labels) {
            return [{
              'color':'0B8043',
              'title':'Дача',
            },
            {
              'color':'0C9CE5',
              'title':'Онеяме',
            },
            {
              'color':'DICK',
              'title':'Аниме',
            }];
          } else if (selector === loginSelectors.userName) {
            return 'usersurname';
          }
          else {
            return 'test';
          }
        });
        const component = mount(<Settings/>);
        expect(component).toMatchSnapshot();
      });
    });
  });

  describe('when labels and username are not provided', () => {
    it('matches the snapshot', () => {
      (useSelector as any).mockImplementation((selector) => {
        if(selector === labelsSelectors.labels) {
          return [];
        } else if(selector === loginSelectors.userName) {
          return null;
        } else {
          return 'token';
        }
      });
      const component = mount(<Settings/>);
      expect(component).toMatchSnapshot();
    });
  });
});