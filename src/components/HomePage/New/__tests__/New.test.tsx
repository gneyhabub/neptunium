/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import {describe, it, jest, expect} from '@jest/globals';
import { mount } from 'enzyme';
import New from '../New';
import { labelsSelectors } from '../../../../__data__/slices/labels';
import { todosSelectors } from '../../../../__data__/slices/list';
import { useSelector } from 'react-redux';
import {labels, todos} from '../../../../../stubs/api/mocks';

const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));

describe('New', () => {
  describe('when todos and labels are provided', () => {
    it('matches the snapshot', () => {
      (useSelector as any).mockImplementation((selector) => {
        if(selector === todosSelectors.todos) {
          return todos;
        } else if(selector === labelsSelectors.labels) {
          return labels;
        } else {
          return 'test';
        }
      });
      const component = mount(<New/>);
      expect(component).toMatchSnapshot();

      let label = component.find('input[name="0B8043"]').simulate('change');
      expect(label).toMatchSnapshot();

      label = component.find('input[name="0B8043"]').simulate('change');
      expect(label).toMatchSnapshot();
    });
  });

  describe('when todos and labels are not provided', () => {
    it('matches the snapshot', () => {
      (useSelector as any).mockImplementation((selector) => {
        if(selector === todosSelectors.todos) {
          return [];
        } else if(selector === labelsSelectors.labels) {
          return [];
        } else if (selector === todosSelectors.error){
          return null;
        } 
        else {
          return 'test';
        }
      });
      const component = mount(<New/>);
      expect(component).toMatchSnapshot();
    });
  });
});