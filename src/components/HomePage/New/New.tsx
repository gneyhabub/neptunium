import React from 'react';
import { useForm } from 'react-hook-form';
import './New.scss';
import { useDispatch, useSelector } from 'react-redux';
import { labelsSelectors } from '../../../__data__/slices/labels';
import labelsAPI from '../../../__data__/api/labels';
import { loginSelectors } from '../../../__data__/slices/login';
import todoAPI from '../../../__data__/api/todos';
import { todosActions, todosSelectors } from '../../../__data__/slices/list';
import { useHistory } from 'react-router-dom';
import { urls } from '../../../__data__/constants/urls';
import { Todo } from '../../../types';

export default function New() {
  const history = useHistory();
  const{ register, handleSubmit, errors, formState: {isSubmitted, isDirty,},} = useForm();
  const labels = useSelector(labelsSelectors.labels);
  const dispatch = useDispatch();
  const accessToken = useSelector(loginSelectors.accessToken);
  const todoIsAdded = useSelector(todosSelectors.isAdded);

  React.useEffect(() => {
    if(!labels.length) dispatch(labelsAPI.getLabels(accessToken));
    dispatch(todosActions.resetFetchStatuses());
    return () => {
      dispatch(todosActions.resetError());
    };
  }, []);

  const [currentLabels, setCurrentLabels] = React.useState(new Array(labels.length).fill(false));
  React.useEffect(() => {
    if(todoIsAdded && isSubmitted) history.push(urls['home.list']);
  }, [todoIsAdded]);

  const toggleLabel = (label) => {
    const exists = currentLabels.find(el => el.color === label.color);
    if(exists) setCurrentLabels(currentLabels.filter(el => el.color !== label.color));
    else setCurrentLabels([...currentLabels, label]);
  };

  const onSubmit = (data: Todo) => {
    dispatch(todoAPI.addTodo({...data, labels: currentLabels.filter((value) => value !== false),}, accessToken));
  };

  return (
    <>
      <div className='main'>
        <h1 className='myh'>Создать новую TODO</h1>
        <form onSubmit={handleSubmit(onSubmit)} className="myform">

          <label className='head' htmlFor='header'>Заголовок</label>
          <input name="title" 
            ref={register({
              required: 'Title is required.',
            })} 
            type='text' 
            id='header' 
            style={{
              border: errors.title && errors.title.message ? '1px red solid' : '',
              outline: 'none',
            }}
          />

          <label className='complete_date' htmlFor='complete_date'>Дата завершения</label>
          <input name="complete_date" 
            ref={register({
              required: 'complete_date is required.',
            })} 
            type='date' 
            id='complete_date'
            style={{
              border: errors.complete_date && errors.complete_date.message ? '1px red solid' : '',
              outline: 'none',
            }}
          />

          <label className='description' htmlFor='description'>Описание</label>
          <textarea id='description' 
            name="description" 
            ref={register()}
          ></textarea>

          <label className='tags' htmlFor='tags'>Тэги</label>
          <div className='tag-container'>
            {labels.map(label => (
              <div key={label.color}>
                <input 
                  type='checkbox'
                  name={label.color}
                  id={label.color}
                  defaultChecked={currentLabels.find(el => el.color === label.color) !== undefined} 
                  onChange={() => toggleLabel(label)}
                />
                <label
                  htmlFor={label.color}
                  style={{
                    color: currentLabels.find(el => el.color === label.color) !== undefined ? 
                      'white' : `#${label.color}`,
                    backgroundColor: currentLabels.find(el => el.color === label.color) !== undefined ? 
                      `#${label.color}` : 'transparent',
                    border: currentLabels.find(el => el.color === label.color) !== undefined ? 
                      '' : `3px #${label.color} solid`,
                  }}
                >{label.title}</label>
              </div>
            ))}
          </div> 
          <div className="new-footer">
            <button type="submit" className='mybutton' style={{
              backgroundColor: !isDirty ? 'grey' : '',
            }}>Create</button>
          </div>
        </form>
      </div>
    </>
  );
}
