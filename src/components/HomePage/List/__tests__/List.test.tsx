/* eslint-disable @typescript-eslint/no-explicit-any */
import {shallow, mount} from 'enzyme';
import React from 'react';
import {describe, it, xit, expect, jest} from '@jest/globals';
import List from '../List';
import { useSelector } from 'react-redux';
import { loginSelectors } from '../../../../__data__/slices/login';
import { todosSelectors } from '../../../../__data__/slices/list';
import { labelsSelectors } from '../../../../__data__/slices/labels';

const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));

describe('List', () => {
  describe('when todos and labels are provided', () => {
    it('matches the snapshot', () => {
      (useSelector as any).mockImplementation((el) => {
        if(el === todosSelectors.todos) {
          return [{title: 'title',}];
        } else if(el === labelsSelectors.labels) {
          return [
            {
              'color':'D50101',
              'title':'Важное',
            },
            {
              'color':'F4511E',
              'title':'Дом',
            }];
        } else if(el === loginSelectors.accessToken) {
          return 'token';
        } else if(el === labelsSelectors.isFetched) {
          return true;
        } else if(el === todosSelectors.isFetched) {
          return true;
        } else if(el === todosSelectors.isUpdated) {
          return true;
        }
      });
      const component = shallow(<List/>);
      expect(component).toMatchSnapshot();
    });
  });

  describe('when todos and labels are not provided', () => {
    xit('matches the snapshot', () => {
      (useSelector as any).mockImplementation((el) => {
        if(el === todosSelectors.todos) {
          return [];
        } else if(el === labelsSelectors.labels) {
          return [];
        } else if(el === loginSelectors.accessToken) {
          return 'token';
        } else if(el === labelsSelectors.isFetched) {
          return true;
        } else if(el === todosSelectors.isFetched) {
          return true;
        } else if(el === todosSelectors.isUpdated) {
          return true;
        }
      });
      mount(<List/>);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
    });
  });
});