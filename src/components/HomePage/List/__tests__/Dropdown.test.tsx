/* eslint-disable @typescript-eslint/no-explicit-any */
import {mount} from 'enzyme';
import React from 'react';
import {describe, it, expect, jest} from '@jest/globals';
import Dropdown from '../Dropdown';

describe('Dropdown', () => {
  const mockSelect = jest.fn();
  const props = {
    list: [{title:'title', color: 'fff',}],
    onSelect: () => mockSelect(),
  };
  const component = mount(<Dropdown {...props}/>);
  it('matches the snapshot', () => {
    expect(component).toMatchSnapshot();
  });

  it('calls onSelect', () => {
    component.find('button').simulate('click');
    component.update();
    component.find('.List-header__dropdown-item').simulate('click');
    expect(mockSelect).toBeCalledTimes(1);
  });
});