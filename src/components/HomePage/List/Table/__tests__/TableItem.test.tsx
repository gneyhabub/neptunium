import TableItem  from '../TableItem';
import React from 'react';
import {describe, it, jest, expect} from '@jest/globals';
import { mount } from 'enzyme';

const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));

describe('TableItem', () => {
  it('matches the snapshot', () => {
    const component = mount(<table>
      <tbody>
        <TableItem todo={{
          'id':'1',
          'completed':false,
          'title':'Съесть деда',
          'labels':[
            {
              'color':'D50101',
              'title':'Важное',
            },
            {
              'color':'F4511E',
              'title':'Дом',
            }
          ],
          'description':'Lorem ipsum dolor sit amet.',
          'completeDate': new Date(5678000),
        }}
        idx={1}
        />
      </tbody>
    </table>);

    expect(component).toMatchSnapshot();
  });

  describe('when todo is completed', () => {
    it('matches the snapshot', () => {
      const component = mount(<table>
        <tbody>
          <TableItem todo={{
            'id':'1',
            'completed': true,
            'title':'Съесть деда',
            'labels':[
              {
                'color':'D50101',
                'title':'Важное',
              },
              {
                'color':'F4511E',
                'title':'Дом',
              }
            ],
            'description':'Lorem ipsum dolor sit amet.',
            'completeDate': new Date(567800000000),
          }}
          idx={1}
          />
        </tbody>
      </table>);
  
      expect(component).toMatchSnapshot();
    });
  });

  describe('when todo is outdated', () => {
    it('matches the snapshot', () => {
      const component = mount(<table>
        <tbody>
          <TableItem todo={{
            'id':'1',
            'completed': false,
            'title':'Съесть деда',
            'labels':[
              {
                'color':'D50101',
                'title':'Важное',
              },
              {
                'color':'F4511E',
                'title':'Дом',
              }
            ],
            'description':'Lorem ipsum dolor sit amet.',
            'completeDate': new Date(5678000000000000000000000000000000),
          }}
          idx={1}
          />
        </tbody>
      </table>);
  
      expect(component).toMatchSnapshot();
    });
  });
});