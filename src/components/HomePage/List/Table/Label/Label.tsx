import React from 'react';
import './Label.scss';

export interface LabelProps {
  color: string;
  title: string;
  key: number;
}

export default function Label({color, title,}: LabelProps) {
  return (
    <div 
      className="Label-sm" 
      style={{
        backgroundColor: `#${color}`,
      }}
      data-title={title}
    >
    </div>
  );
}
