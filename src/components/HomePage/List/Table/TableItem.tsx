import { Todo } from '../../../../types';
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import {faTrashAlt} from '@fortawesome/free-regular-svg-icons';
import Label from './Label/Label';
import './Label/Label.scss';
import todosAPI from '../../../../__data__/api/todos';
import { useDispatch, useSelector } from 'react-redux';
import { loginSelectors } from '../../../../__data__/slices/login';
import { NavLink } from 'react-router-dom';
import { urls } from '../../../../__data__/constants/urls';

export interface TableItemProps {
  todo: Todo;
  idx: number
}

const TableItem: React.FC<TableItemProps> = ({ todo, idx,}: TableItemProps) =>  {
  const dispatch = useDispatch();
  const accessToken = useSelector(loginSelectors.accessToken);
  const onDoneToggle = () => {
    dispatch(todosAPI.updateTodo({...todo, completed: !todo.completed,}, accessToken));
  };
  const onDelete = () => {
    dispatch(todosAPI.deleteTodo(todo.id, accessToken));
  };

  return (
    <tr style={{backgroundColor: todo.completed ? 'rgba(87,101,181, 0.2)': todo.completeDate < new Date() ? 'rgba(222,36,17, 0.2)' : '',}}>
      <td style={{paddingLeft: '20px',}}><input type="checkbox" className="checkbox" checked={todo.completed} onChange={onDoneToggle}></input></td>
      <td>#{idx}</td>
      <td>{todo.title}</td>
      <td>{new Date(todo.completeDate).toDateString()}</td>
      <td className="Label-row">{todo.labels.map((label, idx) => (
        <Label color={label.color} title={label.title} key={idx}></Label>
      ))}</td>
      <td><NavLink to={`${urls['home.list.edit']}/${todo.id}`}>
        <FontAwesomeIcon icon={faEdit} color={'#081C5D'} size="lg"></FontAwesomeIcon>
      </NavLink></td>
      <td><FontAwesomeIcon className="delete-icon" onClick={onDelete} icon={faTrashAlt} color={'#DE2411'} size="lg"></FontAwesomeIcon></td>
    </tr>
  );
};

export default TableItem;
