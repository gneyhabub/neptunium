import { Todo } from 'src/types';
import React from 'react';
import TableItem from './TableItem';
import './Table.scss';

export interface TableProps {
  items: Todo[]
}

export default function Table({items,}: TableProps) {
  return (
    <table className="Table">
      <thead>
        <tr>
          <th style={{paddingLeft: '20px', width: '70px',}} >Done</th>
          <th>TODO</th>
          <th>Title</th>
          <th>Complete Date</th>
          <th>Labels</th>
          <th> </th>
          <th> </th>
        </tr>
      </thead>
      <tbody>
        {items.map((item, idx) => 
          (<TableItem todo={item} idx={idx + 1} key={item.id}></TableItem>)
        )}
      </tbody>
    </table>
  );
}
