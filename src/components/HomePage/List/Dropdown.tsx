import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Label } from 'src/types';
import React from 'react';

export interface DropdownProps {
  list: Label[],
  onSelect: (list: Label[]) => void;
}

export default function Dropdown({list,onSelect,}: DropdownProps) {
  const [isOpen, setIsOpen] = React.useState(false);
  const [labelList, setLabelList] = React.useState(list.map(item => ({selected: false, label: item,})));
  const selectItem = (item: {selected: boolean, label: Label}) => {
    const filteredLabelList = [...labelList];
    filteredLabelList.find(el => el.label === item.label).selected = !filteredLabelList.find(el => el.label === item.label).selected;
    setLabelList(filteredLabelList);
    onSelect(filteredLabelList.filter(label => label.selected).map(item => item.label));
  };
  return (
    <div style={{width: '250px',}}>
      <button className="List-header__dropdown" onClick={() =>setIsOpen(state => !state)}>
          Filter by labels 
        <FontAwesomeIcon className="List-header__dropdown__icon" icon={isOpen ? faAngleUp : faAngleDown}/>
      </button>
      {isOpen && (
        <div
          role="list"
          className="List-header__dropdown-list"
        >
          {labelList.map((item) => (
            <button
              type="button"
              className={'List-header__dropdown-item'}
              key={item.label.color}
              style={{
                color: item.selected? 
                  'white' : `#${item.label.color}`,
                backgroundColor: item.selected ? 
                  `#${item.label.color}` : 'transparent',
                border: item.selected ? 
                  '' : `2px #${item.label.color} solid`,
              }}
              onClick={() => selectItem(item)}
            >
              {item.label.title}
              {' '}
            </button>
          ))}
        </div>
      )}
    </div>
  );
}
