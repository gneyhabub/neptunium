import React  from 'react';
import Table from './Table/Table';
import './List.scss';
import {NavLink} from 'react-router-dom';
import { Label, Todo } from '../../../types';
import Dropdown from './Dropdown';
import { useDispatch, useSelector } from 'react-redux';
import todosAPI from '../../../__data__/api/todos';
import labelsAPI from '../../../__data__/api/labels';
import { todosSelectors } from '../../../__data__/slices/list';
import { loginSelectors } from '../../../__data__/slices/login';
import { labelsSelectors } from '../../../__data__/slices/labels';
import {  urls } from '../../../__data__/constants/urls';

export default function List() {
  const dispatch = useDispatch();
  const todos = useSelector(todosSelectors.todos);
  const labels = useSelector(labelsSelectors.labels);
  const labelsAreFetched = useSelector(labelsSelectors.isFetched);
  const todosAreFetched = useSelector(todosSelectors.isFetched);
  const todosAreUpdated = useSelector(todosSelectors.isUpdated);
  const accessToken = useSelector(loginSelectors.accessToken);
  React.useEffect(() => {
    if(!todos.length) dispatch(todosAPI.getTodos(accessToken));
    if(!labels.length) dispatch(labelsAPI.getLabels(accessToken));
  }, []);

  const [filteredTodos, setFilteredTodos] = React.useState(todos);
  const [titleFilter, setTitleFilter] = React.useState('');
  const [labelFilter, setLabelFilter] = React.useState([]);

  const filterByTitle = (todos: Todo[], titlef: string) => {
    return todos.filter(item => 
      item.title.toLowerCase().includes(titlef.trim().toLowerCase()));
  };
  const filterByLabel = (todos: Todo[], labelf: Label[]) => {
    if (labelf.length === 0) return todos;
    else {
      return (todos.filter(todo => todo.labels.filter((n) => labelf.some(m => n.color === m.color)).length > 0));
    }
  };

  const onFilter = (titlef: string | undefined = undefined, labelf: Label[] | undefined = undefined) => {
    setFilteredTodos(
      filterByLabel(
        filterByTitle(todos, titlef === undefined ? titleFilter : titlef), labelf || labelFilter));
  };

  React.useEffect(() => {
    setFilteredTodos(todos);
    onFilter();
  }, [todosAreFetched, todosAreUpdated, todos]);
  
  return (
    <div className="List">
      <div className="List-header">
        <input 
          placeholder="Title search" 
          type="text" 
          onChange={(e) => {setTitleFilter(e.target.value); onFilter(e.target.value, undefined);}}
        ></input>
        {labelsAreFetched && <Dropdown list={labels} onSelect={list => {setLabelFilter(list); onFilter(undefined, list);}}/>}
        <NavLink className="primary-button" to={urls['home.list.new']}>Create new TODO</NavLink>
      </div>
      {todos.length ? <Table items={filteredTodos}/> : 'You have no todos. Click the button above to create one!'}
    </div>
  );
}
