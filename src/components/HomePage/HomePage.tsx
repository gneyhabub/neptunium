import React from 'react';
import './HomePage.scss';
import { Switch, Route } from 'react-router-dom';
import Settings from './Settings/Settings';
import Nav from './Nav/Nav';
import New from './New/New';
import Header from './Header/Header';
import NotFound from '../NotFound/NotFound';
import List from './List/List';
import { urls } from '../../__data__/constants/urls';
import Edit from './Edit/Edit';
import { useDispatch, useSelector } from 'react-redux';
import { todosSelectors, todosActions } from '../../__data__/slices/list';
import { loginSelectors, loginActions } from '../../__data__/slices/login';
import { labelsSelectors, labelsActions } from '../../__data__/slices/labels';
import Error from './Error/Error';

export default function HomePage() {
  const dispatch = useDispatch();
  const todoError = useSelector(todosSelectors.error);
  const labelsError = useSelector(labelsSelectors.error);
  const loginError = useSelector(loginSelectors.error);
  const onErrorClose = () => {
    dispatch(todosActions.resetError());
    dispatch(labelsActions.resetError());
    dispatch(loginActions.resetError());
  };
  return (
    <div className="HomePage">
      <Header></Header>
      <div className="HomePage-body">
        <Nav />
        <div className="HomePage-body__content">
          <Switch>
            <Route
              exact
              path={urls['home.list']}
              component={List}
            />
            <Route
              exact
              path={urls['home.settings']}
              component={Settings}
            />
            <Route
              exact
              path={urls['home.list.new']}
              component={New}
            />
            <Route
              exact
              path={`${urls['home.list.edit']}/:id`}
              component={Edit}
            />
            <Route
              component={NotFound}
            />
          </Switch>
        </div>
      </div>
      {(todoError || labelsError || loginError) && 
        <Error message={todoError || labelsError || loginError} onClose={onErrorClose} />}
    </div>
  );
}
