/* eslint-disable @typescript-eslint/no-explicit-any */
import {mount, shallow} from 'enzyme';
import React from 'react';
import {describe, it, expect, jest} from '@jest/globals';
import HomePage from '../HomePage';
import { useSelector } from 'react-redux';
import { loginSelectors } from '../../../__data__/slices/login';
import { todosSelectors } from '../../../__data__/slices/list';
import { labelsSelectors } from '../../../__data__/slices/labels';

const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));

describe('HomePage', () => {
  it('matches the snapshot', () => {
    const component = shallow(<HomePage />);
    expect(component).toMatchSnapshot();
  });

  describe('when there is a login error', () => {
    it('dispatches actions on error close', () => {
      (useSelector as any).mockImplementation((selector) => {
        if(selector === loginSelectors.error) {
          return 'error';
        } else {
          return null;
        }
      });
      const component = mount(<HomePage />);
      component.find('.close-icon').at(0).simulate('click');
      expect(mockDispatch).toHaveBeenCalledTimes(4);
    });
  });

  describe('when there is a todo error', () => {
    it('dispatches actions on error close', () => {
      (useSelector as any).mockImplementation((selector) => {
        if(selector === todosSelectors.error) {
          return 'error';
        } else {
          return null;
        }
      });
      const component = mount(<HomePage />);
      component.find('.close-icon').at(0).simulate('click');
      expect(mockDispatch).toHaveBeenCalledTimes(4);
    });
  });

  describe('when there is a labels error', () => {
    it('dispatches actions on error close', () => {
      (useSelector as any).mockImplementation((selector) => {
        if(selector === labelsSelectors.error) {
          return 'error';
        } else {
          return null;
        }
      });
      const component = mount(<HomePage />);
      component.find('.close-icon').at(0).simulate('click');
      expect(mockDispatch).toHaveBeenCalledTimes(4);
    });
  });
});
