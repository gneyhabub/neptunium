/* eslint-disable @typescript-eslint/no-explicit-any */
import {shallow} from 'enzyme';
import React from 'react';
import {describe, it, expect, jest} from '@jest/globals';
import Nav from '../Nav';

const mockDispatch = jest.fn();
const mockHistory = jest.fn();
jest.mock('react-redux', () => ({
  useDispatch: () => mockDispatch,
}));
jest.mock('react-router', () => ({
  useHistory: () => mockHistory,
}));

describe('Nav', () => {
  it('matches the snapshot', () => {
    const component = shallow(<Nav/>);
    expect(component).toMatchSnapshot();
  });
});