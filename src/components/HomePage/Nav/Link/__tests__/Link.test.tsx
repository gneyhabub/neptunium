import React from 'react';
import Link from '../Link';
import {describe, it, expect} from '@jest/globals';
import { shallow } from 'enzyme';

describe('Link', () => {
  it('matches the snapshot', () => {
    const component = shallow(<Link path="/nahui" title="NAHUI"/>);
    expect(component).toMatchSnapshot();
  });
});