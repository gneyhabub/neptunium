import {  urls } from '../../../../__data__/constants/urls';
import React from 'react';
import { NavLink } from 'react-router-dom';
import './Link.scss';

export interface LinkProps {
  path: string;
  title: string;
}

const Link: React.FC<LinkProps> = ({path, title, children,}) =>  {
  return (
    <NavLink 
      className="NavLink"
      activeStyle={{backgroundColor: 'rgba(28, 56, 150, 0.2)',}}
      to={urls[path]}
    >
      <div className="NavLink-content">
        {children}
        <div className="NavLink-title" >{title}</div>
      </div>
    </NavLink>
  );
};

export default Link;