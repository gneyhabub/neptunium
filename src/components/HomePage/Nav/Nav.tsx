import React from 'react';
import Link from './Link/Link';
import './Link/Link.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faList, faCog, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { useHistory } from 'react-router';
import { urls } from '../../../__data__/constants/urls';
import { useDispatch } from 'react-redux';
import { loginActions } from '../../../__data__/slices/login';


export default function Nav() {
  const history = useHistory();
  const dispatch = useDispatch();
  const onLogout = () => {
    dispatch(loginActions.logOut());
    localStorage.removeItem('accessToken');
    history.push(urls['login']);
  };
  return (
    <aside className="HomePage-body__nav">
      <Link path="home.list" title="Home" >
        <FontAwesomeIcon className="NavLink-icon" icon={faList} size="lg"/>
      </Link>
      <Link path="home.settings" title="Settings">
        <FontAwesomeIcon className="NavLink-icon" icon={faCog} size="lg" />
      </Link>
      <button 
        className="NavLink logout-button" 
        onClick={onLogout}
        style={{
          backgroundColor: 'transparent',
          border: 'none',
          width: '90%',
        }}
      >
        <div className="NavLink-content">
          {<FontAwesomeIcon className="NavLink-icon" icon={faSignOutAlt} size="lg"/>}
          <div className="NavLink-title" >Log Out</div>
        </div>
      </button>
    </aside>
  );
}
