/* eslint-disable @typescript-eslint/no-explicit-any */
import {mount} from 'enzyme';
import React from 'react';
import {describe, it, expect, jest} from '@jest/globals';
import Header from '../Header';
import { useSelector } from 'react-redux';
import { loginSelectors } from '../../../../__data__/slices/login';

const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));

describe('Header', () => {
  describe('when username and token are provided', () => {
    it('matches the snapshot', () => {
      (useSelector as any).mockImplementation((el) => {
        if(el === loginSelectors.userName) {
          return 'user';
        } else if(el === loginSelectors.accessToken) {
          return 'token';
        }
      });
      const component = mount(<Header/>);
      expect(component).toMatchSnapshot();
    });
    it('does not call dispatch', () => {
      (useSelector as any).mockImplementation((el) => {
        if(el === loginSelectors.userName) {
          return 'user';
        } else if(el === loginSelectors.accessToken) {
          return 'token';
        }
      });
      mount(<Header/>);
      expect(mockDispatch).toHaveBeenCalledTimes(0);
    });
  });

  describe('when username and token are not provided', () => {
    it('matches the snapshot', () => {
      (useSelector as any).mockImplementation((el) => {
        if(el === loginSelectors.userName) {
          return null;
        } else if(el === loginSelectors.accessToken) {
          return null;
        }
      });
      const component = mount(<Header/>);
      expect(component).toMatchSnapshot();
    });

    it('does not call dispatch', () => {
      (useSelector as any).mockImplementation((el) => {
        if(el === loginSelectors.userName) {
          return null;
        } else if(el === loginSelectors.accessToken) {
          return null;
        }
      });
      mount(<Header/>);
      expect(mockDispatch).toHaveBeenCalledTimes(1);
    });
  });
});