/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import { useForm } from 'react-hook-form';
import './Edit.scss';
import {
  useHistory,
  useParams
} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { todosActions, todosSelectors } from '../../../__data__/slices/list';
import { Todo } from '../../../types';
import todoAPI from '../../../__data__/api/todos';
import labelsAPI from '../../../__data__/api/labels';
import { loginSelectors } from '../../../__data__/slices/login';
import ProgressBar from '../../../components/common/ProgressBar/ProgressBar';
import { labelsSelectors } from '../../../__data__/slices/labels';
import { urls } from '../../../__data__/constants/urls';

export default function Edit() {
  const history = useHistory();
  const{ register, handleSubmit, errors, formState: {isSubmitted,},} = useForm();
  const {id,}: any = useParams();
  const dispatch = useDispatch();
  const labels = useSelector(labelsSelectors.labels);
  const accessToken = useSelector(loginSelectors.accessToken);
  const todo: Todo = useSelector(todosSelectors.todos).find(el => el.id == id);
  const todosAreUpdated = useSelector(todosSelectors.isUpdated);

  React.useEffect(() => {
    if(!todo) {
      dispatch(todoAPI.getTodos(accessToken));
    }
    if(!labels.length) {
      dispatch(labelsAPI.getLabels(accessToken));
    }
    if(todo) setCurrentLabels([...todo.labels]);
    dispatch(todosActions.resetFetchStatuses());
    return () => {
      dispatch(todosActions.resetError());
    };
  }, []);
  const [currentLabels, setCurrentLabels] = React.useState([]);
  React.useEffect(() => {
    if(todosAreUpdated && isSubmitted) history.push(urls['home.list']);
  }, [todosAreUpdated]);

  const toggleLabel = (label) => {
    const exists = currentLabels.find(el => el.color === label.color);
    if(exists) setCurrentLabels(currentLabels.filter(el => el.color !== label.color));
    else setCurrentLabels([...currentLabels, label]);
  };

  const onSubmit = (data) => {
    dispatch(todoAPI.updateTodo({...data, 
      id: todo.id, 
      labels: currentLabels.filter((value) => value !== false),
      completeDate: Date.parse(data.completeDate),
    }, accessToken));
  };

  return (
    <>
      {todo && labels.length ? <div className='edit-wrapper'>
        <h1 className='edit-title'>Edit TODO #{todo.id}</h1>
        <form onSubmit={handleSubmit(onSubmit)} className="edit-form">

          <label className='edit-heading' htmlFor='header'>Заголовок</label>
          <input 
            name="title" 
            ref={register({
              required: 'Title is required.',
            })} 
            type='text' 
            id='header' 
            defaultValue={todo.title}
            style={{
              border: errors.title && errors.title.message ? '1px red solid' : '',
              outline: 'none',
            }}
          />

          <label className='edit-deadline' htmlFor='deadline'>Deadline</label>
          <input 
            name="completeDate" 
            ref={register({
              required: 'Deadline is required.',
            })} 
            type='date' 
            id='deadline'
            defaultValue={new Date(todo.completeDate).toJSON().slice(0,10)}
            style={{
              border: errors.completeDate && errors.completeDate.message ? '1px red solid' : '',
              outline: 'none',
            }}
          />

          <label className='edit-description' htmlFor='description'>Description</label>
          <textarea 
            id='description' 
            name="description" 
            ref={register()} 
            defaultValue={todo.description}
          />

          <label className='edit-tags' htmlFor='tags'>Labels</label>
          <div className='tag-container'>
            {labels.map(label => (
              <div key={label.color}>
                <input 
                  type='checkbox'
                  name={label.color}
                  id={label.color}
                  checked={currentLabels.find(el => el.color === label.color) !== undefined} 
                  onChange={() => toggleLabel(label)}
                />
                <label
                  htmlFor={label.color}
                  style={{
                    color: currentLabels.find(el => el.color === label.color) !== undefined ? 
                      'white' : `#${label.color}`,
                    backgroundColor: currentLabels.find(el => el.color === label.color) !== undefined ? 
                      `#${label.color}` : 'transparent',
                    border: currentLabels.find(el => el.color === label.color) !== undefined ? 
                      '' : `3px #${label.color} solid`,
                  }}
                >{label.title}</label>
              </div>
            ))}
          </div> 

          <div className="edit-footer">
            <button type="submit" className='edit-submit'>Save</button>
          </div>
        </form>
      </div> : <ProgressBar/>}
    </>
  );
}
