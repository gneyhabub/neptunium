/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import {describe, it, jest, expect} from '@jest/globals';
import Edit from '../Edit';
import { mount } from 'enzyme';
import { todosSelectors } from '../../../../__data__/slices/list';
import { labelsSelectors } from '../../../../__data__/slices/labels';
import todoAPI from '../../../../__data__/api/todos';
import labelsAPI from '../../../../__data__/api/labels';
import {useSelector} from 'react-redux';

const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));

jest.mock('../../../../__data__/api/todos', () => ({
  getTodos: (arg) => arg,
  updateTodo: (arg) => arg,
}));

jest.mock('../../../../__data__/api/labels', () => ({
  getLabels: (arg) => arg,
}));

describe('Edit', () => {
  it('matches the snapshot', () => {
    (useSelector as any).mockImplementation((selector) => {
      if(selector === todosSelectors.todos) {
        return [{
          'id':'1',
          'completed':false,
          'title':'Съесть деда',
          'labels':[
            {
              'color':'D50101',
              'title':'Важное',
            },
            {
              'color':'F4511E',
              'title':'Дом',
            }
          ],
          'description':'Lorem ipsum dolor sit amet.',
          'completeDate':567800000000,
        }];
      } else if(selector === labelsSelectors.labels) {
        return [{
          'color':'0B8043',
          'title':'Дача',
        },
        {
          'color':'0C9CE5',
          'title':'Аниме',
        }];
      } else {
        return 'test';
      }
    });
    const component = mount(<Edit/>);
    expect(component).toMatchSnapshot();

    component.find('input[name="0B8043"]').simulate('change');
    expect(component).toMatchSnapshot();

    component.find('input[name="0B8043"]').simulate('change');
    expect(component).toMatchSnapshot();
  });
  describe('when todo and labels are not in storage', () => {
    it('dispatches needed actions', () => {
      (useSelector as any).mockImplementation((selector) => {
        if(selector === todosSelectors.todos) {
          return [];
        } else if(selector === labelsSelectors.labels) {
          return [];
        } else if(selector === todosSelectors.error) {
          return null;
        } else {
          return 'test';
        }
      });
  
      mount(<Edit/>);
      expect(mockDispatch).toHaveBeenCalledWith(todoAPI.getTodos('test'));
      expect(mockDispatch).toHaveBeenCalledWith(labelsAPI.getLabels('test'));
    });
  });
  
});