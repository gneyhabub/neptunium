import { faTimesCircle} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import './Error.scss';

export default function Error({message, onClose,}) {
  return (
    <div className="error-wrapper">
      <p>{message}</p>
      <FontAwesomeIcon className="close-icon" onClick={onClose} icon={faTimesCircle} color={'white'} size="lg"></FontAwesomeIcon>
    </div>
  );
}
