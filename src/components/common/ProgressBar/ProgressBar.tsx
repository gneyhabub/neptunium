import React from 'react';

export default function ProgressBar() {
  return (
    <div style={{ 
      width: '100%', 
      height: 500,
      display: 'flex',
      justifyContent:'center',
      alignItems: 'center',
      flexDirection:'column',
      fontSize: '30px',
    }}>
      <div>Loading...</div>
      <progress
        style={{
          color: 'rgba(28,56,150,1)', 
        }}
        max="100"
      />
    </div>
  );
}
