import ProgressBar from '../ProgressBar';
import {mount} from 'enzyme';
import React from 'react';
import {describe, it, expect} from '@jest/globals';

describe('ProgressBar', () => {
  it('matches the snapshot', () => {
    const component = mount(<ProgressBar />);
    expect(component).toMatchSnapshot();
  });
});