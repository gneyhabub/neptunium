import { configureStore, combineReducers } from '@reduxjs/toolkit';
import { labelsReducer } from './slices/labels';
import { todosReducer } from './slices/list';
import { loginReducer } from './slices/login';

const store =  configureStore({
  reducer: combineReducers({
    login: loginReducer,
    todos: todosReducer,
    labels: labelsReducer,
  }),
});

export type RootState = ReturnType<typeof store.getState>
export default store;