export const API_URL = {
  login: '/login',
  todos: '/todos',
  todo: '/todo',
  labels: '/labels',
  user: '/user',
  signup: '/signup',
};