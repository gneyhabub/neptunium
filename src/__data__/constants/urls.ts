import {getNavigations} from '@ijl/cli';

const navigation = getNavigations('neptunium');

const baseUrl = navigation['neptunium'];
const urls = {
  neptunium: navigation['link.neptunium'],
  login: navigation['link.neptunium.login'],
  signup: navigation['link.neptunium.signup'],
  home: navigation['link.neptunium.home'],
  'home.list': navigation['link.neptunium.home.list'],
  'home.settings': navigation['link.neptunium.home.settings'],
  'home.list.new': navigation['link.neptunium.home.list.new'],
  'home.list.edit': navigation['link.neptunium.home.list.edit'],
};

export {urls, baseUrl};