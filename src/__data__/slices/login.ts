import {createSelector, createSlice} from '@reduxjs/toolkit';
import { rootSelector } from '../root-selector';

type LoginState = {
  username: string,
  email: string,
  accessToken: string,
  loading: boolean,
  error: string
}

export const initialState: LoginState = {
  username: null,
  email: null,
  accessToken: null,
  loading: false,
  error: null,
};

const slice = createSlice({
  name: 'login',
  initialState,
  reducers: {
    signupRequest(state){
      state.loading = true;
      state.error = null;
    },
    signupSuccess(state, {payload: {username, email, accessToken,},}){
      state.loading = false;
      state.username = username;
      state.email = email;
      state.accessToken = accessToken;
    },
    signupFailure(state, {payload,}){
      state.loading = false;
      state.error = payload;
    },
    loginRequest(state){
      state.loading = true;
      state.error = null;
    },
    loginSuccess(state, {payload: {username, accessToken,},}){
      state.loading = false;
      state.username = username;
      state.accessToken = accessToken;
    },
    loginFailure(state, {payload,}){
      state.loading = false;
      state.error = payload;
    },
    getUserRequest(state){
      state.loading = true;
      state.error = null;
    },
    getUserSuccess(state, {payload: {username, email, accessToken,},}){
      state.loading = false;
      state.username = username;
      state.email = email;
      state.accessToken = accessToken;
    },
    getUserFailure(state, {payload,}){
      state.loading = false;
      state.error = payload;
    },
    logOut(state) {
      state.username = null;
      state.email = null;
      state.accessToken = null;
      state.loading = false;
      state.error = null;
    },
    updateUserNameRequest(state) {
      state.loading = true;
      state.error = null;
    },
    updateUserNameSuccess(state, {payload,}) {
      state.username = payload;
      state.loading = false;
      state.error = null;
    },
    updateUserNameFailure(state, {payload,}) {
      state.loading = false;
      state.error = payload;
    },

    updatePasswordRequest(state) {
      state.loading = true;
      state.error = null;
    },
    updatePasswordSuccess(state) {
      state.loading = false;
      state.error = null;
    },
    updatePasswordFailure(state, {payload,}) {
      state.loading = false;
      state.error = payload;
    },
    resetError(state) {
      state.error = null;
    },
  },
});

const rootLoginSelector = createSelector(rootSelector, state => state.login);
const loginSelectors = {
  loading: createSelector(rootLoginSelector, state => state.loading),
  accessToken: createSelector(rootLoginSelector, state => state.accessToken),
  userName: createSelector(rootLoginSelector, state => state.username),
  error: createSelector(rootLoginSelector, state => state.error),
};

const {actions: loginActions, reducer: loginReducer,} = slice;

export {loginActions, loginReducer, loginSelectors};