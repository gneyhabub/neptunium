import {createSelector, createSlice} from '@reduxjs/toolkit';
import { Label } from 'src/types';
import { rootSelector } from '../root-selector';

type LabelState = {
  labels: Label[],
  isFetched: boolean,
  error: string
}

export const initialState : LabelState= {
  labels: [],
  isFetched: false,
  error: null,
};

const slice = createSlice({
  name: 'labels',
  initialState,
  reducers: {
    getLabelsRequest(state){
      state.isFetched = false;
      state.error = null;
    },
    getLabelsSuccess(state, {payload: {labels,},}){
      state.labels = labels;
      state.isFetched = true;
    },
    getLabelsFailure(state, {payload,}){
      state.isFetched = false;
      state.error = payload;
    },

    updateLabelsRequest(state){
      state.isFetched = false;
      state.error = null;
    },
    updateLabelsSuccess(state, {payload,}){
      state.labels = payload;
      state.isFetched = true;
    },
    updateLabelsFailure(state, {payload,}){
      state.isFetched = false;
      state.error = payload;
    },
    resetError(state) {
      state.error = null;
    },
  },
});

const {actions: labelsActions, reducer: labelsReducer,} = slice;

const rootLabelsSelector = createSelector(rootSelector, state => state.labels);
const labelsSelectors = {
  isFetched: createSelector(rootLabelsSelector, state => state.isFetched),
  labels: createSelector(rootLabelsSelector, state => state.labels),
  error: createSelector(rootLabelsSelector, state => state.error),
};

export {labelsActions, labelsSelectors,labelsReducer};