import {createSelector, createSlice} from '@reduxjs/toolkit';
import { Todo } from 'src/types';
import { rootSelector } from '../root-selector';

type ListState = {
  todos: Todo[],
  isFetched: boolean,
  isUpdated: boolean,
  isAdded: boolean,
  isDeleted: boolean;
  error: Error,
}

export const initialState : ListState= {
  todos: [],
  isFetched: false,
  error: null,
  isUpdated: false,
  isAdded: false,
  isDeleted: false,
};

const slice = createSlice({
  name: 'list',
  initialState,
  reducers: {
    getListRequest(state){
      state.isFetched = false;
      state.error = null;
    },
    getListSuccess(state, {payload: {todos,},}){
      state.todos = todos;
      state.isFetched = true;
    },
    getListFailure(state, {payload,}){
      state.isFetched = false;
      state.error = payload;
    },
    updateTodoRequest(state) {
      state.isUpdated = false;
      state.error = null;
    },
    updateTodoSuccess(state, {payload: {todo,},}){
      state.todos = state.todos.map(el => {
        if(el.id === todo.id) return todo; 
        else return el;
      });
      state.isUpdated = true;
    },
    updateTodoFailure(state, {payload,}){
      state.isUpdated = false;
      state.error = payload;
    },

    addTodoRequest(state) {
      state.isAdded = false;
      state.error = null;
    },
    addTodoSuccess(state, {payload: {todo, id,},}){
      state.todos.push({
        id: id,
        completed: todo.completed,
        title: todo.title,
        labels: todo.labels,
        description: todo.description,
        completeDate: todo.complete_date,
      });
      state.isAdded = true;
    },
    addTodoFailure(state, {payload,}){
      state.isAdded = false;
      state.error = payload;
    },

    deleteTodoRequest(state) {
      state.isDeleted = false;
      state.error = null;
    },

    deleteTodoSuccess(state, {payload,}){
      state.todos = state.todos.filter(el => el.id !== payload);
      state.isDeleted = true;
    },
    deleteTodoFailure(state, {payload,}){
      state.isDeleted = false;
      state.error = payload;
    },

    resetFetchStatuses(state){
      state.isUpdated = false;
      state.isFetched = false;
      state.isAdded = false;
      state.isDeleted = false;
    },
    resetError(state) {
      state.error = null;
    },
  },
});

const {actions: todosActions, reducer: todosReducer,} = slice;

const rootTodosSelector = createSelector(rootSelector, state => state.todos);
const todosSelectors = {
  isFetched: createSelector(rootTodosSelector, state => state.isFetched),
  todos: createSelector(rootTodosSelector, state => state.todos),
  isUpdated: createSelector(rootTodosSelector, state => state.isUpdated),
  isAdded: createSelector(rootTodosSelector, state => state.isAdded),
  isDeleted: createSelector(rootTodosSelector, state => state.isDeleted),
  error: createSelector(rootTodosSelector, state => state.error),
};

export {todosActions, todosSelectors, todosReducer};