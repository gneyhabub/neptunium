import {initialState as loginState} from '../login';
import {initialState as labelsState} from '../labels';
import {describe, it, expect} from '@jest/globals';
import {initialState, todosActions, todosSelectors, todosReducer} from '../list';
import {todos} from '../../../../stubs/api/mocks';

describe('list slice', () => {
  describe('selectors', () => {
    it('returns correct entities', () => {
      const rootState = {labels: labelsState, login: loginState, todos: initialState,};
      expect(todosSelectors.isAdded(rootState)).toEqual(false);
      expect(todosSelectors.isDeleted(rootState)).toEqual(false);
      expect(todosSelectors.isFetched(rootState)).toEqual(false);
      expect(todosSelectors.isUpdated(rootState)).toEqual(false);
      expect(todosSelectors.todos(rootState)).toEqual([]);
    });
  });

  describe('getList', () => {
    it('dipatches getListRequest correctly', () => {
      const nextState = {...initialState,};
      const result = todosReducer(initialState, todosActions.getListRequest);
      expect(result).toEqual(nextState);
    });
    it('dipatches getListSuccess correctly', () => {
      const nextState = {...initialState, todos, isFetched: true,};
      const result = todosReducer(initialState, todosActions.getListSuccess({todos,}));
      expect(result).toEqual(nextState);
    });
    it('dipatches getListFailure correctly', () => {
      const nextState = {...initialState, error: 'Error',};
      const result = todosReducer(initialState, todosActions.getListFailure('Error'));
      expect(result).toEqual(nextState);
    });
  });

  describe('updateTodo', () => {
    it('dipatches updateTodoRequest correctly', () => {
      const nextState = {...initialState,};
      const result = todosReducer(initialState, todosActions.updateTodoRequest);
      expect(result).toEqual(nextState);
    });
    it('dipatches updateTodoSuccess correctly', () => {
      const updatedTodo = {...todos[0], title: 'Съесть бабку',};
      const newTodos = [...todos];
      newTodos[0] = updatedTodo;
      const nextState = {...initialState, isUpdated: true, todos: newTodos,};
      const result = todosReducer({...initialState, todos,}, todosActions.updateTodoSuccess({todo: 
        updatedTodo,}));
      expect(result).toEqual(nextState);
    });
    it('dipatches updateTodoFailure correctly', () => {
      const nextState = {...initialState, error: 'Error',};
      const result = todosReducer(initialState, todosActions.updateTodoFailure('Error'));
      expect(result).toEqual(nextState);
    });
  });

  describe('addTodo', () => {
    it('dipatches addTodoRequest correctly', () => {
      const nextState = {...initialState,};
      const result = todosReducer(initialState, todosActions.addTodoRequest);
      expect(result).toEqual(nextState);
    });
    it('dipatches addTodoSuccess correctly', () => {
      const newTodo = todos[0];
      const nextState = {...initialState, isAdded: true, todos: [
        newTodo
      ],};
      const result = todosReducer(initialState, todosActions.addTodoSuccess({todo: {...newTodo, complete_date: 567800000000, }, id: 0, }));
      expect(result).toEqual(nextState);
    });
    it('dipatches addTodoFailure correctly', () => {
      const nextState = {...initialState, error: 'Error',};
      const result = todosReducer(initialState, todosActions.addTodoFailure('Error'));
      expect(result).toEqual(nextState);
    });
  });

  describe('deleteTodo', () => {
    it('dipatches deleteTodoRequest correctly', () => {
      const nextState = {...initialState,};
      const result = todosReducer(initialState, todosActions.deleteTodoRequest);
      expect(result).toEqual(nextState);
    });
    it('dipatches deleteTodoSuccess correctly', () => {
      const nextState = {...initialState, isDeleted: true, todos: [],};
      const result = todosReducer({...initialState, todos: [todos[0]],}, todosActions.deleteTodoSuccess(0));
      expect(result).toEqual(nextState);
    });
    it('dipatches deleteTodoFailure correctly', () => {
      const nextState = {...initialState, error: 'Error',};
      const result = todosReducer(initialState, todosActions.deleteTodoFailure('Error'));
      expect(result).toEqual(nextState);
    });
  });

  describe('resetFetchStatuses', () => {
    it('resetes fetch statues', () => {
      const result = todosReducer(initialState, todosActions.resetFetchStatuses);
      expect(result).toEqual(initialState);
    });
  });
});