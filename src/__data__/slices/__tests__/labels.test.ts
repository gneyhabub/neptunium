import {describe, it, expect} from '@jest/globals';
import {initialState, labelsActions, labelsSelectors, labelsReducer} from '../labels';
import {initialState as loginState} from '../login';
import {initialState as todoState} from '../list';
import {labels} from '../../../../stubs/api/mocks';
describe('Labels slice', () => {
  describe('getLabels', () => {
    it('dipatches getLabelsRequest correctly', () => {
      const nextState = {...initialState,};
      const result = labelsReducer(initialState, labelsActions.getLabelsRequest);
      expect(result).toEqual(nextState);
    });
    it('dipatches getLabelsSuccess correctly', () => {
      const nextState = {...initialState, labels, isFetched: true,};
      const result = labelsReducer(initialState, labelsActions.getLabelsSuccess({labels,}));
      expect(result).toEqual(nextState);
    });
    it('dipatches getLabelsFailure correctly', () => {
      const nextState = {...initialState, error: 'Mi obosralis',};
      const result = labelsReducer(initialState, labelsActions.getLabelsFailure('Mi obosralis'));
      expect(result).toEqual(nextState);
    });
  });

  describe('updateLabels', () => {
    it('dipatches updateLabelsRequest correctly', () => {
      const nextState = {...initialState,};
      const result = labelsReducer(initialState, labelsActions.updateLabelsRequest);
      expect(result).toEqual(nextState);
    });
    it('dipatches updateLabelsSuccess correctly', () => {
      const nextState = {...initialState, labels, isFetched: true,};
      const result = labelsReducer(initialState, labelsActions.updateLabelsSuccess(labels));
      expect(result).toEqual(nextState);
    });
    it('dipatches updateLabelsRequest correctly', () => {
      const nextState = {...initialState, error: 'Mi obosralis',};
      const result = labelsReducer(initialState, labelsActions.updateLabelsFailure('Mi obosralis'));
      expect(result).toEqual(nextState);
    });
  });

  describe('selectors', () => {
    it('returns correct entities', () => {
      expect(labelsSelectors.isFetched({labels: initialState, login: loginState, todos: todoState,})).toEqual(false);
      expect(labelsSelectors.labels({labels: initialState, login: loginState, todos: todoState,})).toEqual([]);
    });
  });
});