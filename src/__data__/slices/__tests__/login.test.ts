import {describe, it, expect} from '@jest/globals';
import {initialState, loginActions, loginSelectors, loginReducer} from '../login';
import {initialState as labelsState} from '../labels';
import {initialState as todoState} from '../list';

describe('login', () => {
  describe('signup', () => {
    it('dipatches signupRequest correctly', () => {
      const nextState = {...initialState, loading: true,};
      const result = loginReducer(initialState, loginActions.signupRequest);
      expect(result).toEqual(nextState);
    });
    it('dipatches signupSuccess correctly', () => {
      const nextState = {...initialState, username:'name', email: 'mail', accessToken: 'token',};
      const result = loginReducer(initialState, loginActions.signupSuccess({username:'name', email: 'mail', accessToken: 'token',}));
      expect(result).toEqual(nextState);
    });
    it('dipatches signupFailure correctly', () => {
      const nextState = {...initialState, error: 'Zhopa',};
      const result = loginReducer(initialState, loginActions.signupFailure('Zhopa'));
      expect(result).toEqual(nextState);
    });
  });

  describe('login', () => {
    it('dipatches sloginRequest correctly', () => {
      const nextState = {...initialState, loading: true,};
      const result = loginReducer(initialState, loginActions.loginRequest);
      expect(result).toEqual(nextState);
    });
    it('dipatches loginSuccess correctly', () => {
      const nextState = {...initialState, username:'name', accessToken: 'token',};
      const result = loginReducer(initialState, loginActions.loginSuccess({username:'name', accessToken: 'token',}));
      expect(result).toEqual(nextState);
    });
    it('dipatches loginFailure correctly', () => {
      const nextState = {...initialState, error: 'Error',};
      const result = loginReducer(initialState, loginActions.loginFailure('Error'));
      expect(result).toEqual(nextState);
    });
  });

  describe('getUser', () => {
    it('dipatches getUserRequest correctly', () => {
      const nextState = {...initialState, loading: true,};
      const result = loginReducer(initialState, loginActions.getUserRequest);
      expect(result).toEqual(nextState);
    });
    it('dipatches getUserSuccess correctly', () => {
      const nextState = {...initialState, username:'name', email: 'emal', accessToken: 'token',};
      const result = loginReducer(initialState, loginActions.getUserSuccess({username:'name', email: 'emal', accessToken: 'token',}));
      expect(result).toEqual(nextState);
    });
    it('dipatches getUserFailure correctly', () => {
      const nextState = {...initialState, error: 'Error',};
      const result = loginReducer(initialState, loginActions.getUserFailure('Error'));
      expect(result).toEqual(nextState);
    });
  });

  describe('updateUserName', () => {
    it('dipatches updateUserNameRequest correctly', () => {
      const nextState = {...initialState, loading: true,};
      const result = loginReducer(initialState, loginActions.updateUserNameRequest);
      expect(result).toEqual(nextState);
    });
    it('dipatches updateUserNameSuccess correctly', () => {
      const nextState = {...initialState, username:'name',};
      const result = loginReducer(initialState, loginActions.updateUserNameSuccess('name'));
      expect(result).toEqual(nextState);
    });
    it('dipatches updateUserNameFailure correctly', () => {
      const nextState = {...initialState, error: 'Error',};
      const result = loginReducer(initialState, loginActions.updateUserNameFailure('Error'));
      expect(result).toEqual(nextState);
    });
  });

  describe('updatePassword', () => {
    it('dipatches updatePasswordRequest correctly', () => {
      const nextState = {...initialState, loading: true,};
      const result = loginReducer(initialState, loginActions.updatePasswordRequest);
      expect(result).toEqual(nextState);
    });
    it('dipatches updatePasswordSuccess correctly', () => {
      const nextState = {...initialState,};
      const result = loginReducer(initialState, loginActions.updatePasswordSuccess);
      expect(result).toEqual(nextState);
    });
    it('dipatches updatePasswordFailure correctly', () => {
      const nextState = {...initialState, error: 'Error',};
      const result = loginReducer(initialState, loginActions.updatePasswordFailure('Error'));
      expect(result).toEqual(nextState);
    });
  });

  describe('logout', () => {
    it('dipatches logout correctly', () => {
      const nextState = {...initialState,};
      const result = loginReducer(initialState, loginActions.logOut);
      expect(result).toEqual(nextState);
    });
  });

  describe('selectors', () => {
    it('returns correct entities', () => {
      const rootState = {labels: labelsState, login: initialState, todos: todoState,};
      expect(loginSelectors.accessToken(rootState)).toEqual(null);
      expect(loginSelectors.error(rootState)).toEqual(null);
      expect(loginSelectors.loading(rootState)).toEqual(false);
      expect(loginSelectors.userName(rootState)).toEqual(null);
    });
  });
});