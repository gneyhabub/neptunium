/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import {getConfigValue} from '@ijl/cli';
import axios from 'axios';
import { Todo } from 'src/types';
import { API_URL } from '../constants/api-urls';
import { todosActions } from '../slices/list';

const getTodos = (accessToken: string) => async (dispatch): Promise<void> => {
  dispatch(todosActions.getListRequest());
  try {
    const {data: {todos,},} = await axios({
      method: 'get',
      url: `${getConfigValue('neptunium.api')}${API_URL.todos}`,
      headers: {'Authorization': `Bearer ${accessToken}`,},
    });
    dispatch(todosActions.getListSuccess({todos,}));
  } catch(e) {
    dispatch(todosActions.getListFailure(e.message || 'Something went wrong'));
  }
};

const updateTodo = (todo: Todo, accessToken: string) => async (dispatch): Promise<void> => {
  dispatch(todosActions.updateTodoRequest());
  try {
    await axios({
      method: 'put',
      url: `${getConfigValue('neptunium.api')}${API_URL.todo}`,
      headers: { 
        'Authorization': `Bearer ${accessToken}`, 
      },
      data: {
        todo,
      },
    });
    dispatch(todosActions.updateTodoSuccess({todo,}));
  } catch(e) {
    dispatch(todosActions.updateTodoFailure(e.message || 'Something went wrong'));
  }
};

const addTodo = (todo: Todo, accessToken: string) => async (dispatch): Promise<void> => {
  dispatch(todosActions.addTodoRequest());
  try {
    const {data,} =  await axios({
      method: 'post',
      url: `${getConfigValue('neptunium.api')}${API_URL.todo}`,
      headers: { 
        'Authorization': `Bearer ${accessToken}`, 
      },
      data: todo,
    });
    dispatch(todosActions.addTodoSuccess({todo,id: data,}));
  } catch(e) {
    dispatch(todosActions.addTodoFailure(e.message || 'Something went wrong'));
  }
};

const deleteTodo = (id: string, accessToken: string) => async (dispatch): Promise<void> => {
  dispatch(todosActions.deleteTodoRequest());
  try {
    await axios({
      method: 'delete',
      url: `${getConfigValue('neptunium.api')}${API_URL.todo}`,
      headers: { 
        'Authorization': `Bearer ${accessToken}`, 
      },
      data: {
        id,
      },
    });
    dispatch(todosActions.deleteTodoSuccess(id));
  } catch(e) {
    dispatch(todosActions.deleteTodoFailure(e.message || 'Something went wrong'));
  }
};

export default {getTodos, updateTodo, addTodo,deleteTodo,};