/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import {getConfigValue} from '@ijl/cli';
import axios from 'axios';
import { API_URL } from '../constants/api-urls';
import { labelsActions } from '../slices/labels';

const getLabels = (accessToken: string) => async (dispatch): Promise<void> => {
  dispatch(labelsActions.getLabelsRequest());
  try {
    const {data,} = await axios({
      method: 'get',
      url: `${getConfigValue('neptunium.api')}${API_URL.labels}`,
      headers: {'Authorization': `Bearer ${accessToken}`,},
    });
    dispatch(labelsActions.getLabelsSuccess({labels: data,}));
  } catch(e) {
    dispatch(labelsActions.getLabelsFailure(e.message || 'Something went wrong'));
  }
};

export default {getLabels,};