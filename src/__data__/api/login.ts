/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import {loginActions} from '../slices/login';
import {getConfigValue} from '@ijl/cli';
import axios from 'axios';
import { API_URL } from '../constants/api-urls';
import { labelsActions } from '../slices/labels';

const login = (username: string, password: string) => async (dispatch): Promise<void> => {
  dispatch(loginActions.loginRequest());
  try {
    const {data,} = await axios({
      method: 'post',
      url: `${getConfigValue('neptunium.api')}${API_URL.login}`,
      data: {
        username,
        password,
      },
    });
    const accessToken = data.accessToken;
    localStorage.setItem('accessToken', accessToken);
    dispatch(loginActions.loginSuccess({username, accessToken,}));
  } catch(e) {
    dispatch(loginActions.loginFailure(e.message === 'Request failed with status code 401' ? 'Wrong password!' : e.message || 'Something went wrong'));
  }
};

const signup = (username: string, password: string, email: string) => async (dispatch): Promise<void> => {
  dispatch(loginActions.signupRequest());
  try {
    const {data,} = await axios({
      method: 'post',
      url: `${getConfigValue('neptunium.api')}${API_URL.signup}`,
      data: {
        username,
        email,
        password,
      },
    });
    const accessToken = data.accessToken;
    localStorage.setItem('accessToken', accessToken);
    dispatch(loginActions.signupSuccess({username, email, accessToken,}));
  } catch(e) {
    dispatch(loginActions.signupFailure(e.message || 'Something went wrong'));
  }
};

const getUser = (accessToken: string) => async (dispatch): Promise<void> => {
  dispatch(loginActions.getUserRequest());
  try {
    const {data: {username, email,},} = await axios({
      method: 'get',
      url: `${getConfigValue('neptunium.api')}${API_URL.user}`,
      headers: {'Authorization': `Bearer ${accessToken}`,},
    });
    dispatch(loginActions.getUserSuccess({username, email, accessToken,}));
  } catch(e) {
    dispatch(loginActions.getUserFailure(e.message || 'Something went wrong'));
  }
};

const updateUserData = (accessToken: string, data: Record<string, string>) => 
  async (dispatch): Promise<void> => {
    if(data.username) {
      dispatch(loginActions.updateUserNameRequest());
      try {
        await axios({
          method: 'put',
          url: `${getConfigValue('neptunium.api')}${API_URL.user}/username`,
          headers: {'Authorization': `Bearer ${accessToken}`,},
          data: {
            username: data.username,
          },
        });
        dispatch(loginActions.updateUserNameSuccess(data.username));
      } catch(e) {
        dispatch(loginActions.updateUserNameFailure(e.message || 'Something went wrong'));
      }
    }

    if(data.password) {
      dispatch(loginActions.updatePasswordRequest());
      try {
        await axios({
          method: 'put',
          url: `${getConfigValue('neptunium.api')}${API_URL.user}/password`,
          headers: {'Authorization': `Bearer ${accessToken}`,},
          data: {
            password: data.password,
          },
        });
        dispatch(loginActions.updatePasswordSuccess());
      } catch(e) {
        dispatch(loginActions.updatePasswordFailure(e.message || 'Something went wrong'));
      }
    }

    if(data.labels) {
      dispatch(labelsActions.updateLabelsRequest());
      try {
        await axios({
          method: 'put',
          url: `${getConfigValue('neptunium.api')}${API_URL.labels}`,
          headers: {'Authorization': `Bearer ${accessToken}`,},
          data: {
            labels: data.labels,
          },
        });
        dispatch(labelsActions.updateLabelsSuccess(data.labels));
      } catch(e) {
        dispatch(labelsActions.updateLabelsFailure(e.message || 'Something went wrong'));
      }
    }
  };

export default {login, signup, getUser, updateUserData,};