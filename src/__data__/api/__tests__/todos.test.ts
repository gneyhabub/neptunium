/* eslint-disable @typescript-eslint/no-explicit-any */
import todosAPI from '../todos';
import {describe, it, expect, jest} from '@jest/globals';
import { todosActions } from '../../slices/list';
import axios from 'axios';

jest.mock('../../slices/list', () => ({
  todosActions: {
    getListRequest: jest.fn(),
    getListSuccess: jest.fn(),
    getListFailure: jest.fn(),
    updateTodoRequest: jest.fn(),
    updateTodoSuccess: jest.fn(),
    updateTodoFailure: jest.fn(),
    addTodoRequest: jest.fn(),
    addTodoSuccess: jest.fn(),
    addTodoFailure: jest.fn(),
    deleteTodoRequest: jest.fn(),
    deleteTodoSuccess: jest.fn(),
    deleteTodoFailure: jest.fn(),
  },
}));
jest.mock('axios', () => jest.fn());

describe('todosAPI', () => {
  const mockDispatch = jest.fn();
  describe('getTodos', () => {
    it('calls getListSuccess on success', async () => {
      (axios as any).mockImplementation(() => Promise.resolve({data: {todos: ['1'],},}));
      await todosAPI.getTodos('token')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(todosActions.getListSuccess).toHaveBeenCalled();
    });

    it('calls getListSuccess on success', async () => {
      (axios as any).mockImplementation(() => {throw new Error();});
      await todosAPI.getTodos('token')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(todosActions.getListFailure).toHaveBeenCalled();
    });
  });

  describe('updateTodo', () => {
    const todo = {id: '1', labels: [{color: 'color', title: 'title',}], completed: false, title: 'title', description: 'aa', completeDate: new Date(),};
    it('calls updateTodosSuccess on success', async () => {
      (axios as any).mockImplementation(() => Promise.resolve());
      await todosAPI.updateTodo(todo, 'token')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(todosActions.updateTodoSuccess).toHaveBeenCalled();
    });

    it('calls updateTodosFailure on failure', async () => {
      (axios as any).mockImplementation(() => {throw new Error();});
      await todosAPI.updateTodo(todo, 'token')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(todosActions.updateTodoFailure).toHaveBeenCalled();
    });
  });

  describe('addTodo', () => {
    const todo = {id: '1', labels: [{color: 'color', title: 'title',}], completed: false, title: 'title', description: 'aa', completeDate: new Date(),};
    it('calls addTodosSuccess on success', async () => {
      (axios as any).mockImplementation(() => Promise.resolve({data: 'id',}));
      await todosAPI.addTodo(todo, 'token')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(todosActions.addTodoSuccess).toHaveBeenCalled();
    });

    it('calls addTodosFailure on failure', async () => {
      (axios as any).mockImplementation(() => {throw new Error();});
      await todosAPI.addTodo(todo, 'token')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(todosActions.addTodoFailure).toHaveBeenCalled();
    });
  });

  describe('deleteTodo', () => {
    it('calls deleteTodosSuccess on success', async () => {
      (axios as any).mockImplementation(() => Promise.resolve({data: 'id',}));
      await todosAPI.deleteTodo('1', 'token')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(todosActions.deleteTodoSuccess).toHaveBeenCalled();
    });

    it('calls deleteTodosFailure on failure', async () => {
      (axios as any).mockImplementation(() => {throw new Error();});
      await todosAPI.deleteTodo('1', 'token')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(todosActions.deleteTodoFailure).toHaveBeenCalled();
    });
  });
});