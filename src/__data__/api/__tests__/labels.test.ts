/* eslint-disable @typescript-eslint/no-explicit-any */
import labelsAPI from '../labels';
import {describe, it, expect, jest} from '@jest/globals';
import { labelsActions } from '../../slices/labels';
import axios from 'axios';

jest.mock('../../slices/labels', () => ({
  labelsActions: {
    getLabelsRequest: jest.fn(),
    getLabelsSuccess: jest.fn(),
    getLabelsFailure: jest.fn(),
    updateLabelsRequest: jest.fn(),
    updateLabelsSuccess: jest.fn(),
    updateLabelsFailure: jest.fn(),
  },
}));
jest.mock('axios', () => jest.fn());

describe('labelsAPI', () => {
  const mockDispatch = jest.fn();
  describe('when the API call is successfull', () => {
    it('calls getLabelsSuccess', async () => {
      (axios as any).mockImplementation(() => Promise.resolve('data'));
      await labelsAPI.getLabels('token')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(labelsActions.getLabelsSuccess).toHaveBeenCalled();
    });
  });

  describe('when the API call is unsuccessfull', () => {
    it('calls getLabelsFailure', async () => {
      (axios as any).mockImplementation(() => {throw new Error();});
      await labelsAPI.getLabels('token')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(labelsActions.getLabelsFailure).toHaveBeenCalled();
    });
  });
});