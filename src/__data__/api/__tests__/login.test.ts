/* eslint-disable @typescript-eslint/no-explicit-any */
import loginAPI from '../login';
import {describe, it, expect, jest} from '@jest/globals';
import { loginActions } from '../../slices/login';
import axios from 'axios';
import { labelsActions } from '../../slices/labels';

jest.mock('../../slices/login', () => ({
  loginActions: {
    loginRequest: jest.fn(),
    loginSuccess: jest.fn(),
    loginFailure: jest.fn(),
    signupRequest: jest.fn(),
    signupSuccess: jest.fn(),
    signupFailure: jest.fn(),
    getUserRequest: jest.fn(),
    getUserSuccess: jest.fn(),
    getUserFailure: jest.fn(),
    updateUserNameRequest: jest.fn(),
    updateUserNameSuccess: jest.fn(),
    updateUserNameFailure: jest.fn(),
    updatePasswordRequest: jest.fn(),
    updatePasswordSuccess: jest.fn(),
    updatePasswordFailure: jest.fn(),
  },
}));
jest.mock('../../slices/labels', () => ({
  labelsActions: {
    updateLabelsRequest: jest.fn(),
    updateLabelsSuccess: jest.fn(),
    updateLabelsFailure: jest.fn(),
  },
}));
jest.mock('axios', () => jest.fn());

describe('loginAPI', () => {
  const mockDispatch = jest.fn();
  describe('login', () => {
    it('calls loginSuccess on success', async () => {
      (axios as any).mockImplementation(() => Promise.resolve({data: 'token',}));
      await loginAPI.login('user', 'pass')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(loginActions.loginSuccess).toHaveBeenCalled();
    });
    it('calls loginFailure on failure', async () => {
      (axios as any).mockImplementation(() => {throw new Error();});
      await loginAPI.login('user', 'pass')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(loginActions.loginFailure).toHaveBeenCalled();
    });
  });

  describe('signup', () => {
    it('calls signupSuccess on success', async () => {
      (axios as any).mockImplementation(() => Promise.resolve({data: 'token',}));
      await loginAPI.signup('user', 'pass', 'eliam')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(loginActions.signupSuccess).toHaveBeenCalled();
    });
    it('calls signupFailure on failure', async () => {
      (axios as any).mockImplementation(() => {throw new Error();});
      await loginAPI.signup('user', 'pass', 'eliam')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(loginActions.signupFailure).toHaveBeenCalled();
    });
  });

  describe('getUser', () => {
    it('calls getUserSuccess on success', async () => {
      (axios as any).mockImplementation(() => Promise.resolve({data: {username: 'name', email: 'mail',},}));
      await loginAPI.getUser('token')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(loginActions.getUserSuccess).toHaveBeenCalled();
    });
    it('calls getUserFailure on failure', async () => {
      (axios as any).mockImplementation(() => {throw new Error();});
      await loginAPI.getUser('token')(mockDispatch);
      expect(mockDispatch).toHaveBeenCalledTimes(2);
      expect(loginActions.getUserFailure).toHaveBeenCalled();
    });
  });

  describe('updateUserData', () => {
    describe('when username is provided', () => {
      it('calls updateUserNameSuccess on success', async () => {
        (axios as any).mockImplementation(() => Promise.resolve());
        await loginAPI.updateUserData('token', {username: 'name',})(mockDispatch);
        expect(mockDispatch).toHaveBeenCalledTimes(2);
        expect(loginActions.updateUserNameSuccess).toHaveBeenCalled();
      });
      it('calls updateUserNameFailure on failure', async () => {
        (axios as any).mockImplementation(() => {throw new Error();});
        await loginAPI.updateUserData('token', {username: 'name',})(mockDispatch);
        expect(mockDispatch).toHaveBeenCalledTimes(2);
        expect(loginActions.updateUserNameFailure).toHaveBeenCalled();
      });
    });

    describe('when password is provided', () => {
      it('calls updatePasswordSuccess on success', async () => {
        (axios as any).mockImplementation(() => Promise.resolve());
        await loginAPI.updateUserData('token', {password: 'pass',})(mockDispatch);
        expect(mockDispatch).toHaveBeenCalledTimes(2);
        expect(loginActions.updatePasswordSuccess).toHaveBeenCalled();
      });
      it('calls updatePasswordFailure on failure', async () => {
        (axios as any).mockImplementation(() => {throw new Error();});
        await loginAPI.updateUserData('token', {password: 'pass',})(mockDispatch);
        expect(mockDispatch).toHaveBeenCalledTimes(2);
        expect(loginActions.updatePasswordFailure).toHaveBeenCalled();
      });
    });
  
    describe('when labels are provided', () => {
      it('calls updateLabelsSuccess on success', async () => {
        (axios as any).mockImplementation(() => Promise.resolve());
        await loginAPI.updateUserData('token', {labels: 'labels',})(mockDispatch);
        expect(mockDispatch).toHaveBeenCalledTimes(2);
        expect(labelsActions.updateLabelsSuccess).toHaveBeenCalled();
      });
      it('calls updateLabelsFailure on failure', async () => {
        (axios as any).mockImplementation(() => {throw new Error();});
        await loginAPI.updateUserData('token', {labels: 'labels',})(mockDispatch);
        expect(mockDispatch).toHaveBeenCalledTimes(2);
        expect(labelsActions.updateLabelsFailure).toHaveBeenCalled();
      });
    });
  });
});