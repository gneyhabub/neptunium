import React from 'react';
import { Provider } from 'react-redux';
import './App.scss';
import BaseRouter from './BaseRouter';
import initStore from './__data__/store';

const App = () => {
  return (
    <div className="AppWrapper">
      <Provider store={initStore}>
        <BaseRouter/>
      </Provider>
    </div>
  );
};

export default App;
