/* eslint-disable @typescript-eslint/no-explicit-any */
import {shallow} from 'enzyme';
import React from 'react';
import {describe, it, expect, jest} from '@jest/globals';
import BaseRouter from '../BaseRouter';
import { useSelector } from 'react-redux';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
}));

describe('NotFound', () => {
  describe('when the user is logged in', () => {
    it('matches the snapshot', () => {
      (useSelector as any).mockImplementation(() => true);
      const component = shallow(<BaseRouter />);
      expect(component).toMatchSnapshot();
    });
  });

  describe('when the user is not logged in', () => {
    it('matches the snapshot', () => {
      (useSelector as any).mockImplementation(() => false);
      const component = shallow(<BaseRouter />);
      expect(component).toMatchSnapshot();
    });
  });
});